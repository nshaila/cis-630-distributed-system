
#include <iostream>
#include <string.h>
#include <vector>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>


using namespace std;



FILE *inFile, *outFile;
unsigned int totalNode = 0;
unsigned int nodeMax = 0; 
int round;
double credit;
double** creditArray;
int vecSize = 1;
int increment = 1;



// function for printing the time and message for a specific section of code
void printTime(time_t start, string message) {

    time_t stop;
    time(&stop);
    cout << message << " " << difftime(stop, start) << "sec" << endl;
}


// function for reading from file and creating a vector of neighbors for each node
vector< vector<int> > readInputFile(const char* inputFile) {

    time_t startTime;
    time(&startTime);
    
    // open input file to get maximum node
    /*inFile = fopen(inputFile, "r");
    if (inFile != NULL) {
    
        unsigned int i, j;
        while (fscanf(inFile, "%u %u", &i, &j) == 2) {
        
            if (i > nodeMax) nodeMax = i;
            if (j > nodeMax) nodeMax = j;
        }
        fclose(inFile);
    }
    else {
    
        cout << "File not found! '" << inputFile << "'" << endl;
        exit(1);
    }*/
    
    // create neighbors vector of vectors and set it to the max node size
    vector< vector<int> > neighbor(vecSize);
    
    // scan input file and add edge to the neighbors vector
    inFile = fopen(inputFile, "r");
    if (inFile != NULL) {
    
        unsigned int k, l;
        while (fscanf(inFile, "%u %u", &k, &l) == 2) {
        
        	vecSize += increment;
         	neighbor.resize(vecSize);
            neighbor[k-1].push_back(l-1);
            neighbor[l-1].push_back(k-1);
            

        }
        fclose(inFile);
    }
    else {
    
        cout << "File not found! '" << inputFile << "'" << endl;
        exit(1);
    }
    
    printTime(startTime, "time to read input file =");
    
    return neighbor;
}


// function for writing the output file with node names and size of their neighbors. 
void writeOutputFile(vector< vector<int> > neighbor, const char* outputFile) {

	//time_t startTimer;
    //time(&startTimer);
    
    // create a 2d array for credit values
    creditArray = new double*[nodeMax];
    
    for (size_t i = 0; i < nodeMax; i++) {
    
        creditArray[i] = new double[2];
    }
    
    // open output file and write each node and neighbor
    int n = 0;
    outFile = fopen(outputFile, "w");
    if (outFile != NULL) {
    
        for (size_t i = 0; i < nodeMax; i++) {
        
            // initialize credit to 1.0
            if (neighbor[i].size() > 0) {
            
                creditArray[i][0] = 1.0;
                totalNode += 1;
                
                char buffer [50];
                n = sprintf (buffer, "%lu\t%lu\n", (i+1), neighbor[i].size());
                fwrite (buffer, sizeof(char), n, outFile);
            }
            else {
            
                creditArray[i][0] = 0;
                creditArray[i][1] = 0;
            }
        }
        fclose(outFile);
    }
    else {
    
        cout << "Can't open output file '" << outputFile << "'" << endl;
        exit(1);
    }
    //printTime(startTimer, "time to create output file =");
}



// function for the many random walk algorithm. it loops through each node and calculate credit then its appended at
// the end of the output file 
void randomWalk(vector< vector<int> > neighbor, const char* outputFile) {

    int oldCredit = 0;
    int newCredit = 1;
    double totalCredit = 0;
    unsigned int index = 0;
    const string tempFile = "temp";
    
    size_t line = 4096, slen = 0;
    time_t startTime;
  
    // number of rounds set for each run, given as an argument during runtime
    for (int loop = 0; loop < round; loop++) {
    
        time(&startTime);
        
        char fileLine [line];
        
        // open output file to read previous credits
        inFile = fopen(outputFile, "r");
        if (inFile == NULL) {
        
            cout << "Cannot open output file '" << outputFile << "'" << endl;
            exit(1);
        }
        
        // open temp output file to write new credits
        outFile = fopen(tempFile.c_str(), "w");
        if (outFile == NULL) {
        
            cout << "Cannot open temporary file '" << tempFile << "'" << endl;
            exit(1);
        }
        
        size_t i = 0;
        totalCredit = 0;
        
        // scan through each file, append the new credit to the node's line and write it to temp file
        while (fgets(fileLine, line, inFile) != NULL) {
        
            if (i >= nodeMax) {
            
                cout << "i=" << i << " max node size=" << nodeMax << endl;
                exit(1);
            }
            
            // increment i until a node has neighbors
            while (neighbor[i].size() <= 0 && i < nodeMax)
                i++;
            
            // calculating the credit 
            credit = 0;
            for (size_t j = 0; j < neighbor[i].size(); j++) {
            
                index = neighbor[i][j];
                credit += creditArray[index][oldCredit] / neighbor[index].size();
            }
            
            creditArray[i][newCredit] = credit;
            
            slen = strlen(fileLine);
            if (fileLine[slen-1] == '\n')
                fileLine[slen-1] = 0;
            
            // write node's credit for current loop
            fprintf(outFile, "%s\t%f\n", fileLine, credit);
            
            i++;
            totalCredit += credit;
        }
        
        fclose(inFile);
        fclose(outFile);
        //printTime(startTime, "time to write the output file =");
        
        // Double the size for the file line if the current line len is within 100 characters
        if (slen > line - 100) {
        
            line *= 2;
        }
        
        // remove previous output file
        remove(outputFile);
        
        // rename this round's temporary file
        rename(tempFile.c_str(), outputFile);
        
        oldCredit = newCredit;
        newCredit = (newCredit + 1) % 2;
        
        cout << "time for round " << (loop + 1) << " =";
        printTime(startTime, " ");
        
    }
}


// function for validating the input arguments
void validateInput(int argc, char* argv[]) {

    if (argc < 4) {
    
        cout << "Input form % prog infile outfile rounds" << endl;
        exit(1);
    }
    
    round = atoi(argv[3]);
    if (round <= 0) {
    
        cout << "Input a positive integer." << endl;
        exit(1);
    }
}


// function for deallocating memory blocks
void deleteMemory() {

    for (size_t i = 0; i < nodeMax; i++) {
        if (creditArray[i] != NULL) 
        	delete[] creditArray[i];
    }
    
    if (creditArray != NULL) 
    	delete[] creditArray;
}


int main(int argc, char* argv[]) {

    validateInput(argc, argv);

    vector< vector<int> > neighbor = readInputFile(argv[1]);

    writeOutputFile(neighbor, argv[2]);

    randomWalk(neighbor, argv[2]);

    deleteMemory();
    
    return 0;
}

