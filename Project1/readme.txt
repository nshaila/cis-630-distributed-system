
CIS630, Spring 2015, Term Project (Part I), Due date: April 23, 2015

Please complete the following information, save this file and submit it along with your program

Your First and Last name: Nashid Shaila
Your Student ID: 951378156

What programming language did you use to write your code?

C++

Does your program compile on ix: Yes

How should we compile your program on ix (please provide short but specific commands)

g++ mrw.cpp 

./a.out input output rounds

Does your program run on ix: Yes

Does your program have a limit for the number of nodes in the input graph? No
If yes, what is the limit on graph size that your program can handle?

How long does it take for your program to read the input file on ix?

118sec

How long does it take for your program (on average) to complete each round of processing on ix?

51sec