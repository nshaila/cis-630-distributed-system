
CIS630, Spring 2015, Term Project (Part II), Due date: June 2, 2015

Please complete the following information, save this file and submit it along with your program

Your First and Last name: 
Your Student ID:

What programming language did you use to write your code?

Does your program compile on ix-trusty: Yes/No

How should we compile your program on ix-trusty (please provide short but specific commands)

Does your program run on ix-trusty: Yes/No

Does your program generate the correct results with 2 and 4 partitions on ix-trusty: Yes/No

Does your program have a limit for the number of nodes in the input graph? Yes/No
If yes, what is the limit on graph size that your program can handle?

How long does it take for your program to read the input file on ix-trusty?

How long does it take for your program (on average) to complete each round of processing on ix-trusty?