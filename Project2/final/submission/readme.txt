CIS630, Spring 2015, Term Project (Part II), Due date: June 2, 2015

Please complete the following information, save this file and submit it along with your program

Your First and Last name: Nashid Shaila	
Your Student ID: 951378156

What programming language did you use to write your code? C++

Does your program compile on ix-trusty: Yes

How should we compile your program on ix-trusty (please provide short but specific commands)
I have a Makefile with the program, so compile with the command make 

Does your program run on ix-trusty: Yes

Does your program generate the correct results with 2 and 4 partitions on ix-trusty: Yes

Does your program have a limit for the number of nodes in the input graph? No
If yes, what is the limit on graph size that your program can handle?

How long does it take for your program to read the input file on ix-trusty?
Approximately 12.3 seconds for each partition

How long does it take for your program (on average) to complete each round of processing on ix-trusty?

for 2 partitions: ~1.30 seconds total for a round (fl_compact_part.2)
for 4 partitions: ~0.92 seconds total for a round (fl_compact_part.4)

