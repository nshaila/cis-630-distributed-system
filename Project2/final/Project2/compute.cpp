#include "compute.h"

using namespace std;

typedef vector<double> CreditVec;


namespace compute {

/*bool file_exists (const string filename) {

  ifstream ifile(filename);
  return ifile;
}


float time_elapsed(clock_t t1, clock_t t2) {

    float diff((float)t2-(float)t1);
    return diff / CLOCKS_PER_SEC;
}


CreditVec scale(CreditVec &C, double &new_max, double &new_min) {

	CreditVec C_ = C;
	
	double min = *min_element(begin(C), end(C));
	double max = *max_element(begin(C), end(C));

	int i = 0;
	
	for_each (begin(C), end(C), [&](const double d) {
	
		C_[i] = (((d - min) / (max - min + 0.0000001)) * (new_max - new_min)) + new_min;
		++i;
	});

	return C_;
}


CreditVec normalize(CreditVec &C) {

	CreditVec C_ = C;
	double sum = accumulate(begin(C_), end(C_), 0.0);
	
	for (unsigned int i = 0; i < C_.size(); ++i) {
		C_[i] /= sum;
	}

	return C_;
}


double average(CreditVec &C, CreditVec &C_) {

	double accum = 0.0;
	
	for (unsigned int i=0; i<C.size(); ++i) {
		accum += pow(C[i] - C_[i], 2);
	}

	return sqrt(accum / C.size());
}


double stdev(CreditVec &C) {

	double sum = accumulate(begin(C), end(C), 0.0);
	double m =  sum / C.size();

	double accum = 0.0;
	
	for_each (begin(C), end(C), [&](const double d) {
		accum += (d - m) * (d - m);
	});

	return sqrt(accum / (C.size()-1));
}


void save_data(vector<double> &data, string file) {

	ofstream myfile;
 	myfile.open (file);
 	
 	for (auto& d: data) {
 		myfile << d << '\n';
 	}
 	
  	myfile.close();
}


void save_result(vector<CreditVec> &distribs, vector<double> &diff_avg, vector<double> &stdevs) {

	int i = 0;
	
	for (auto& cv: distribs) {
		save_data(cv, "data/distrib" + to_string(++i) + ".dat");
	}
	
	save_data(diff_avg, "data/diffavg.dat");
	save_data(stdevs, "data/stdev.dat");
}*/

} 
