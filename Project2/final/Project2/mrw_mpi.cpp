#include <iostream>
#include <stdio.h>
#include <assert.h>
#include <string>
#include <algorithm>
#include <fstream>

#include "mpi.h"
#include "omp.h"
#include "Node.h"

#define  MASTER		0

using namespace std;



vector<GraphSize> graph; //id of local nodes
NodeVec nodevec;
bool is_master;
int  rounds_num, task_num, taskid;

MPI_Datatype ExtNode_type;
ExtNode *snodes, *rnodes;
int *counts, *display;
size_t size;

bool file_exists (const string filename) {

  ifstream ifile(filename);
  return ifile;
}

void load_network(string edge_file, string partition_file) {

	GraphSize source, target, max, current_size, degree ;
	int partition;
	Node *source_node, *target_node;

	ifstream edgefile(edge_file);
	
	if(edgefile.is_open()) {
	
		while(edgefile >> source >> target) { 
		
			if(source > 0 && target > 0) { 
                
                if(source >= nodevec.size() || target >= nodevec.size()) {
                
                    max = std::max(source, target);
                    current_size = nodevec.size();
                    nodevec.resize(max + 1);
                    
                    while(current_size <= max) {
                        nodevec[current_size++] = new Node();
                    }
                }

                source_node = nodevec[source];
                source_node->setId(source);

                target_node = nodevec[target];
                target_node->setId(target);

                source_node->addEdge(target_node);
                target_node->addEdge(source_node);
            }
		}
	}
	
	edgefile.close();


	ifstream partitionfile(partition_file);
	
	if(partitionfile.is_open()) {
	
		GraphSize cur_index = 0;
		
		while(partitionfile >> source >> degree >> partition) {
		
            if(source > 0) { 
            
                source_node = nodevec[source];
                source_node->setDegree(degree);
                source_node->setPartition(partition);

                if(taskid == partition) {
                
                    graph.push_back(source);
                    source_node->setIndex(cur_index++);
                }
            }
		}
	}
	
	partitionfile.close();
}


void initialize_buffers() {

	Node *node;
	GraphSize partition;

	counts = (int*) malloc( sizeof(int)*task_num );
	display = (int*) malloc( sizeof(int)*task_num );

	for(int i = 0; i < task_num; ++i) {
		counts[i] = 0;
	}
	
	for(auto& id : graph) {
	
		node = nodevec[id];
		
		for(auto& edge_node : *(node->getEdges())) {
		
			partition = edge_node->partition();
			
			if(partition != (unsigned) taskid) {
				counts[partition]++;
			}
		}
	}

	display[0] = 0;
	
	for(int i = 1; i < task_num; ++i) {
		display[i] = counts[i-1] + display[i-1];
	}
	
	size = 0;
	
	for(int i = 0; i < task_num; ++i) {
		size += counts[i];
	}

	snodes = (ExtNode*)malloc(sizeof(ExtNode)*size);
	rnodes = (ExtNode*)malloc(sizeof(ExtNode)*size);
	
	for(size_t i = 0; i < size; ++i) {
		snodes[i] = ExtNode();
	}
	
	for(size_t i = 0; i < size; ++i) {
	
		rnodes[i] = ExtNode();
	}
}



void write_output_file(vector<CreditVec> update) {

	FILE *outfile = fopen((to_string(taskid) + ".out").c_str(), "w" );
	Node *n;
	
   	sort(graph.begin(), graph.end());

	for(auto& id : graph) {
	
		n = nodevec[id];
		fprintf(outfile, "%lu\t%lu", id, n->degree());
		
		for(size_t i = 0; i < update.size(); ++i ) {
			fprintf(outfile, "\t%.6lf", update[i][n->index()]);
		}
		
		fprintf(outfile, "\n");
	}

	fclose(outfile);
}



void exchange_credit_updates() {

	Node *node;
	GraphSize id;
	int partition;

    vector<int> counter;
    
    for(int i = 0; i < task_num; ++i) {
		counter.push_back( display[i] );
	}
    
	for(auto& id : graph) {
	
		node = nodevec[id];
		
		for(auto& target_node : *(node->getEdges())) {
		
			partition = target_node->partition() ;
			
			if(taskid != partition) {
			
				snodes[counter[partition]].id = node->id();
				snodes[counter[partition]].credit = node->credit();
				counter[partition]++;
			}
		}
    }

	MPI_Alltoallv( snodes, counts, display, ExtNode_type, rnodes, counts, display, ExtNode_type, MPI_COMM_WORLD );

	for(size_t i = 0; i < size; ++i) {
	
		id = rnodes[i].id;
		node = nodevec[id];
		node->setCredit(rnodes[i].credit);
	}
}



void update_credit(CreditVec &C) {

	double sum;
	Node *node;
	
	for(size_t i = 0; i < graph.size(); ++i) {
	
		node = nodevec[graph[i]];
		sum = 0;
		
		for(auto& target_node: *(node->getEdges())) {
			sum += target_node->credit() / target_node->degree();
		}
		
		C[node->index()] = sum;
	}

	#pragma omp parallel for private(node)
	for(size_t i = 0; i < graph.size(); ++i) {
	
		node = nodevec[graph[i]];
		node->setCredit( C[node->index()] );
	}
}



int main(int argc, char *argv[]) {

	double start, end, total_start=0, total_end=0;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &task_num);
	MPI_Comm_rank(MPI_COMM_WORLD, &taskid);
	is_master = (taskid == MASTER) ? true : false;

    string partition_file;
	string edge_file;

	if(argc != 4) {
	
		if(is_master) {
			cout << "Input form: mpiexec -np <partitions> ./prog <Graph_file> <Partition_file> <rounds>" << endl;
		}
		
		MPI_Finalize();
		exit(0);
	} 
	
	else {
	
		edge_file = argv[1];
		partition_file = argv[2];
		rounds_num = atoi( argv[3] );
	}

	if(!file_exists(partition_file)) { 
		if(is_master) {
			cout << "Partition file does not exist." << endl;
		}
		
		MPI_Finalize();
		exit(0);
	}
	
	if(!file_exists(edge_file)) {
	 
		if(is_master) {
			cout << "Graph file doesn't exist. " << endl;
		}
		
		MPI_Finalize();
		exit(0);
	}

	start = omp_get_wtime();
	load_network(edge_file, partition_file );
	
	end = omp_get_wtime();
	cout << "time to read input files, partition " << taskid <<  " = " << (end - start) << " seconds" << endl;

    MPI_Datatype types[2] = {MPI_UNSIGNED_LONG, MPI_DOUBLE};
    int b[2] = {1, 1};
    
    MPI_Aint extent, lower_bound;
    MPI_Type_get_extent(MPI_UNSIGNED_LONG, &lower_bound, &extent);
    
    MPI_Aint offsets[2] = {0, 1*extent};
    MPI_Type_create_struct(2, b, offsets, types, &ExtNode_type);
    MPI_Type_commit(&ExtNode_type);

	initialize_buffers();

	MPI_Barrier( MPI_COMM_WORLD );
	
	if(is_master) { 
		cout << "\nCredit Values for " << rounds_num << " Rounds:\n" << endl;
	}

	CreditVec C(graph.size(), 0);
	
	vector<CreditVec> updates(rounds_num);

	for(int i = 0; i < rounds_num; ++i) {
	
		if(is_master) {
			total_start = omp_get_wtime();
		}

		start = omp_get_wtime();
		update_credit(C);

		updates[i] = C;
		end = omp_get_wtime();
		
		cout << "--- time for round " << i+1 << ", partition " << taskid << " = " << (end - start)<< " seconds" << endl;

		MPI_Barrier( MPI_COMM_WORLD );

		exchange_credit_updates();
		
        if(is_master) {
        
			total_end = omp_get_wtime();
			cout << "total time for round "<< i+1 << ": " << (total_end - total_start) << " seconds\n" << endl;
		}
	}

	write_output_file( updates );
	
	free( counts );
	free( display );
	free( snodes );
	free( rnodes );
	for ( auto& node: nodevec ) {
		delete node;
	}
	
	MPI_Finalize();
	
	return 0 ;
}

	
